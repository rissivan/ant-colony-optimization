//
// Created by ivan on 04. 04. 2020..
//

#ifndef ANTCOLONYOPTIMIZATION_CYCLEANT_H
#define ANTCOLONYOPTIMIZATION_CYCLEANT_H


#include <mutex>
#include <condition_variable>
#include "AbstractAnt.h"

class CycleAnt: public AbstractAnt{
protected:
    void antMethod(std::mutex* m, std::condition_variable* cvAnts, std::condition_variable* cvMain, std::condition_variable* cvWait, int noIterations,
            int noAnts, int dim, double alpha, double beta, std::vector<std::vector<std::shared_ptr<double>>> pheromones,
            std::vector<std::vector<std::shared_ptr<double>>> distances, std::shared_ptr<int> sync, std::shared_ptr<int> syncWait) override;
    void printAnt();
public:
    CycleAnt()= default;;
    CycleAnt(int index, int startingNode, std::mutex* m, std::condition_variable* cvAnts, std::condition_variable* cvMain, std::condition_variable* cvWait,
             int noAnts, int noIterations, int dim, double alpha, double beta, std::vector<std::vector<std::shared_ptr<double>>> pheromones,
             std::vector<std::vector<std::shared_ptr<double>>> distances, std::shared_ptr<int> sync, std::shared_ptr<int> syncWait);
};


#endif //ANTCOLONYOPTIMIZATION_CYCLEANT_H
