//
// Created by ivan on 08. 05. 2020..
//

#ifndef ANTCOLONYOPTIMIZATION_STEPANT_H
#define ANTCOLONYOPTIMIZATION_STEPANT_H


#include "AbstractAnt.h"

class StepAnt: public AbstractAnt{
private:
    double q0;
    double phi;
    double tau0;
protected:
    void antMethod(std::mutex *m, std::condition_variable *cvAnts, std::condition_variable *cvMain,
                   std::condition_variable *cvWait, int noIterations, int noAnts, int dim, double alpha, double beta,
                   std::vector<std::vector<std::shared_ptr<double>>> pheromones,
                   std::vector<std::vector<std::shared_ptr<double>>> distances, std::shared_ptr<int> sync,
                   std::shared_ptr<int> syncWait) override;
    void printAnt();
public:
    StepAnt()= default;;
    StepAnt(int index, int startingNode, std::mutex* m, std::condition_variable* cvAnts, std::condition_variable* cvMain, std::condition_variable* cvWait,
            int noIterations, int noAnts, int dim, double alpha, double beta, std::vector<std::vector<std::shared_ptr<double>>> pheromones,
            std::vector<std::vector<std::shared_ptr<double>>> distances, std::shared_ptr<int> sync, std::shared_ptr<int> syncWait, double q0, double phi, double tau0);
};


#endif //ANTCOLONYOPTIMIZATION_STEPANT_H
