//
// Created by ivan on 31. 03. 2020..
//

#ifndef ANTCOLONYOPTIMIZATION_ABSTRACTANT_H
#define ANTCOLONYOPTIMIZATION_ABSTRACTANT_H

#include <thread>
#include <vector>
#include <iostream>
#include <set>
#include <mutex>
#include <condition_variable>

class AbstractAnt {
protected:
    int index{};
    std::set<int> notVisited; //insert je O(logn), erase je O(1)
    std::thread antThread;
    std::vector<int> path;
    double length{};
    int startingNode{};
    virtual void antMethod(std::mutex* m, std::condition_variable* cvAnts, std::condition_variable* cvMain, std::condition_variable* cvWait, int noIterations,
                           int noAnts, int dim, double alpha, double beta, std::vector<std::vector<std::shared_ptr<double>>> pheromones,
                           std::vector<std::vector<std::shared_ptr<double>>> distances, std::shared_ptr<int> sync, std::shared_ptr<int> syncWait)=0;
    void fillSet(int dim, const std::vector<int>&);
public:
    AbstractAnt()= default;;
    virtual ~AbstractAnt();
    [[nodiscard]] const std::vector<int> &getPath() const;
    [[nodiscard]] double getLength() const;
    [[nodiscard]] int getIndex() const;
};


#endif //ANTCOLONYOPTIMIZATION_ABSTRACTANT_H
