//
// Created by ivan on 08. 05. 2020..
//

#include <iostream>
#include <random>
#include <cmath>
#include "StepAnt.h"

using namespace std;

void StepAnt::antMethod(std::mutex *m, std::condition_variable *cvAnts, std::condition_variable *cvMain,
                        std::condition_variable *cvWait, int noIterations, int noAnts, int dim, double alpha,
                        double beta, std::vector<std::vector<std::shared_ptr<double>>> pheromones,
                        std::vector<std::vector<std::shared_ptr<double>>> distances, std::shared_ptr<int> sync,
                        std::shared_ptr<int> syncWait) {
    random_device rd;
    mt19937_64 engine(rd());
    uniform_real_distribution<double> dist(0, 1);


    for(int iteration = 0; iteration < noIterations; iteration++){

        {
            unique_lock<mutex> lock(*m);
            *syncWait = *syncWait + 1;
            if(*syncWait != noAnts)
                cvWait->wait(lock, [syncWait, noAnts]{return *syncWait == noAnts;});
            else
                cvWait->notify_all();
        }


        this->path.clear();
        this->length = 0;
        int currentNode = this->startingNode;
        this->path.push_back(currentNode);
        this->fillSet(dim, this->path);

        while(!this->notVisited.empty()){

            double q = dist(engine);
            int nextNode = 0;

            if(q > q0){
                double totalWeight = 0;
                vector<double> weights;
                int index = 0;


                for(int node : this->notVisited){
                    while(index < node){
                        weights.push_back(0);
                        index++;
                    }
                    double weight = pow (*pheromones[currentNode][node], alpha) * pow((100 / *distances[currentNode][node]), beta);
                    weights.push_back(weight);
                    totalWeight += weight;
                    index++;
                }

                double randomNumber = dist(engine);
                double currentProbability = 0;

                for(double weight: weights){
                    double probability = weight / totalWeight;
                    currentProbability += probability;
                    if(randomNumber <= currentProbability)
                        break;
                    nextNode++;
                }
            } else {
                double max = 0;
                for(int node: this->notVisited){
                    double product = *pheromones[currentNode][node] * pow((100 / *distances[currentNode][node]), beta);
                    if(product > max){
                        max = product;
                        nextNode = node;
                    }
                }
            }

            path.push_back(nextNode);
            this->length += *distances[currentNode][nextNode];
            this->fillSet(dim, path);


            {
                lock_guard<mutex> lg(*m);
                double newValue = (1 - this->phi) * *pheromones[currentNode][nextNode] + this->phi * this->tau0;
                *pheromones[currentNode][nextNode] = newValue;
                *pheromones[nextNode][currentNode] = newValue;
            }

            currentNode = nextNode;
        }

        this->path.push_back(this->startingNode);
        this->length += *distances[currentNode][this->startingNode];

        {
            lock_guard<mutex> lg(*m);
            double newValue = (1 - this->phi) * *pheromones[currentNode][this->startingNode] + this->phi * this->tau0;
            *pheromones[currentNode][this->startingNode] = newValue;
            *pheromones[this->startingNode][currentNode] = newValue;
        }

        {
            unique_lock<mutex> lock(*m);
            printAnt();
            *sync = *sync + 1;
            cvMain->notify_one();
            cvAnts->wait(lock, [sync]{return *sync == 0;});
        }

    }
}

StepAnt::StepAnt(int index, int startingNode, std::mutex *m, std::condition_variable *cvAnts,
                 std::condition_variable *cvMain, std::condition_variable *cvWait, int noIterations, int noAnts,
                 int dim, double alpha, double beta, std::vector<std::vector<std::shared_ptr<double>>> pheromones,
                 std::vector<std::vector<std::shared_ptr<double>>> distances, std::shared_ptr<int> sync,
                 std::shared_ptr<int> syncWait, double q0, double phi, double tau0) {
    this->index = index;
    this->startingNode = startingNode;
    this->length = 0;
    this->q0 = q0;
    this->phi = phi;
    this->tau0 = tau0;
    this->antThread = thread(&StepAnt::antMethod, this, m, cvAnts, cvMain, cvWait, noIterations, noAnts, dim, alpha, beta, pheromones, distances, sync, syncWait);
}

void StepAnt::printAnt() {
    cout << "Ant " << this->index << " found path of length " << this->length << ": [";
    for(int p: path){
        cout << p << " ";
    }
    cout << "]" << endl;
}
