//
// Created by ivan on 31. 03. 2020..
//

#include "AbstractAnt.h"

AbstractAnt::~AbstractAnt() {
    if(this->antThread.joinable()){
        this->antThread.join();
//        std::cout << "Ant " << this->index << " joined." << std::endl;
    }
}

const std::vector<int> &AbstractAnt::getPath() const {
    return path;
}

double AbstractAnt::getLength() const {
    return length;
}


void AbstractAnt::fillSet(int dim, const std::vector<int>& visited) {
    std::set<int> visitedSet;

    this->notVisited.clear();

    for(int v: visited){
        visitedSet.insert(v);
    }

    int nodeIndex = 0;
    for(int v: visitedSet){
        while(nodeIndex < v){
            this->notVisited.insert(nodeIndex);
            nodeIndex++;
        }
        nodeIndex = v + 1;
    }

    for(; nodeIndex < dim; nodeIndex++){
        this->notVisited.insert(nodeIndex);
    }
}

int AbstractAnt::getIndex() const {
    return index;
}
