//
// Created by ivan on 04. 04. 2020..
//

#include <iostream>
#include <random>
#include <cmath>
#include "CycleAnt.h"

using namespace std;


void CycleAnt::antMethod(mutex* m, condition_variable* cvAnts, condition_variable* cvMain, condition_variable* cvWait, int noIterations, int noAnts, int dim, double alpha,
                         double beta, vector<vector<shared_ptr<double>>> pheromones, vector<vector<shared_ptr<double>>> distances, shared_ptr<int> sync, shared_ptr<int> syncWait) {
    random_device rd;
    mt19937_64 engine(rd());
    uniform_real_distribution<double> dist(0, 1);

    for(int iteration = 0; iteration < noIterations; iteration++){

        {
            unique_lock<mutex> lock(*m);
            *syncWait = *syncWait + 1;
            if(*syncWait != noAnts)
                cvWait->wait(lock, [syncWait, noAnts]{return *syncWait == noAnts;});
            else
                cvWait->notify_all();
        }

        this->path.clear();
        this->length = 0;
        int currentNode = this->startingNode;
        this->path.push_back(currentNode);
        this->fillSet(dim, this->path);

        while(!this->notVisited.empty()){

            double totalWeight = 0;
            vector<double> weights;
            int index = 0;

            for(int node : this->notVisited){
                while(index < node){
                    weights.push_back(0);
                    index++;
                }
                double weight = pow (*pheromones[currentNode][node], alpha) * pow((100 / *distances[currentNode][node]), beta);
                weights.push_back(weight);
                totalWeight += weight;
                index++;
            }

            double randomNumber = dist(engine);
            double currentProbability = 0;
            int nextNode = 0;

            for(double weight: weights){
                double probability = weight / totalWeight;
                currentProbability += probability;
                if(randomNumber <= currentProbability)
                    break;
                nextNode++;
            }
            path.push_back(nextNode);
            this->length += *distances[currentNode][nextNode];
            this->fillSet(dim, path);

            currentNode = nextNode;
        }

        this->path.push_back(this->startingNode);
        this->length += *distances[currentNode][this->startingNode];

        {
            unique_lock<mutex> lock(*m);
            printAnt();
            *sync = *sync + 1;
            cvMain->notify_one();
            cvAnts->wait(lock, [sync]{return *sync == 0;});
        }

    }
}

CycleAnt::CycleAnt(int index, int startingNode, mutex* m, condition_variable* cvAnts, condition_variable* cvMain, condition_variable* cvWait, int noIterations, int noAnts, int dim,
                   double alpha, double beta, vector<vector<shared_ptr<double>>> pheromones, vector<vector<shared_ptr<double>>> distances, shared_ptr<int> sync, shared_ptr<int> syncWait) {
    this->index = index;
    this->startingNode = startingNode;
    this->length = 0;
    this->antThread = thread(&CycleAnt::antMethod, this, m, cvAnts, cvMain, cvWait, noIterations, noAnts, dim, alpha, beta, pheromones, distances, sync, syncWait);
}

void CycleAnt::printAnt() {
    cout << "Ant " << this->index << " found path of length " << this->length << ": [";
    for(int p: path){
        cout << p << " ";
    }
    cout << "]" << endl;
}
