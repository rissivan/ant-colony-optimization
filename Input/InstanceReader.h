//
// Created by ivan on 31. 03. 2020..
//

#ifndef ANTCOLONYOPTIMIZATION_INSTANCEREADER_H
#define ANTCOLONYOPTIMIZATION_INSTANCEREADER_H

#include <iostream>
#include <fstream>
#include <vector>
#include <string>

class InstanceReader {
private:
    std::vector<std::pair<double, double>> coordinates;
    std::string path;
    void loadData();
public:
    InstanceReader()= default;;
    explicit InstanceReader(std::string path);
    [[nodiscard]] const std::vector<std::pair<double, double>> &getCoordinates() const;
    ~InstanceReader()= default;;
};


#endif //ANTCOLONYOPTIMIZATION_INSTANCEREADER_H
