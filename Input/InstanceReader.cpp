//
// Created by ivan on 31. 03. 2020..
//

#include "InstanceReader.h"

#include <utility>

using namespace std;

vector<string> split(const string& str, const string& delimiter){
    vector<string> tokens;
    size_t previous = 0, position = 0;
    int len = str.length();
    do{
        position = str.find(delimiter, previous);
        if (position == string::npos)
            position = str.length();
        string token = str.substr(previous, position - previous);
        if(!token.empty())
            tokens.push_back(token);
        previous = position + delimiter.length();
    } while (position < len && previous < len);
    return tokens;
}

string trim(const string& str)
{
    size_t first = str.find_first_not_of(' ');
    if (string::npos == first)
    {
        return str;
    }
    size_t last = str.find_last_not_of(' ');
    return str.substr(first, (last - first + 1));
}

InstanceReader::InstanceReader(std::string path) {
    this->path = std::move(path);
    loadData();
}

void InstanceReader::loadData() {
    cout << "Loading input data from \"" + path + "\"" << endl;
    string line;
    ifstream input_file(path);
    if(input_file){
        while(getline(input_file, line)){
            if (line == "EOF")
                break;
            line = trim(line);
            if(line[0] < '0' || line[0] > '9')
                continue;
            vector<string> tokens = split(line, " ");
            this->coordinates.emplace_back(stod(tokens[1]), stod(tokens[2]));

        }
    } else {
        printf ("Unable to open file.\n");
        exit(1);
    }
    cout << "Successfully loaded " << coordinates.size() << " cities." << endl << endl;
}

const vector<std::pair<double, double>> &InstanceReader::getCoordinates() const {
    return coordinates;
}

