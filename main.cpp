#include "Problem/TravellingSalesmanProblem.h"
#include "Ant/CycleAnt.h"
#include "Algorithm/AntSystem.h"
#include "Algorithm/ElitistAntSystem.h"
#include "Algorithm/RankBasedAntSystem.h"
#include "Algorithm/MaxMinAntSystem.h"
#include "Algorithm/AntColonySystem.h"

using namespace std;

int main() {
    TravellingSalesmanProblem tsp("../Data/dj38.tsp");
    AntColonySystem antColonySystem(tsp);

    antColonySystem.setNumberOfAnts(30);
    antColonySystem.setNumberOfIterations(100);
    antColonySystem.setAlpha(1.0);
    antColonySystem.setBeta(2.0);
    antColonySystem.setRho(0.1);
    antColonySystem.setQ0(0.9);
    antColonySystem.setPhi(0.1);

    antColonySystem.run();
    antColonySystem.printStats();

    return 0;
}
