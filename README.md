# Ant Colony Optimization

Završni rad na temu "Radni okvir za rješavanje problema trgovačkog putnika paralelnim algoritmom kolonije mrava"

Za pokretanje algoritma nad željenim problemom trgovačkog putnika (konkretne probleme preuzeti s: http://elib.zib.de/pub/mp-testdata/tsp/tsplib/tsp/index.html ili http://www.math.uwaterloo.ca/tsp/world/countries.html),
potrebno je u datoteci ***main.cpp*** navesti put do istog, npr:

    TravellingSalesmanProblem tsp("../Data/dj38.tsp");

Potom je potrebno stvoriti instancu algoritma nad navedenim problemom:

    AntColonySystem antColonySystem(tsp);

Nadalje, moguće je konfigurirati parametre instanciranog algoritma na sljedeći način:

    antColonySystem.setNumberOfAnts(30);
    antColonySystem.setNumberOfIterations(100);
    antColonySystem.setAlpha(1.0);
    antColonySystem.setBeta(2.0);
    antColonySystem.setRho(0.1);
    antColonySystem.setQ0(0.9);
    antColonySystem.setPhi(0.1);
     
Na kraju, algoritam se pokreće i ispisuje potrebne informacije naredbama:


    antColonySystem.run();
    antColonySystem.printStats();