//
// Created by ivan on 31. 03. 2020..
//

#ifndef ANTCOLONYOPTIMIZATION_TRAVELLINGSALESMANPROBLEM_H
#define ANTCOLONYOPTIMIZATION_TRAVELLINGSALESMANPROBLEM_H

#include <vector>
#include <memory>
#include "../Input/InstanceReader.h"

class TravellingSalesmanProblem {
private:
    std::vector<std::vector<std::shared_ptr<double>>> cityDistances;
    std::vector<std::vector<std::shared_ptr<double>>> pheromones;
    int dim{};
    double initialPheromone;
    InstanceReader reader;
    void loadData(std::vector<std::pair<double, double>> coordinates);
    static double distance(double x1, double y1, double x2, double y2);
public:
    TravellingSalesmanProblem()= default;;
    explicit TravellingSalesmanProblem(std::string path);
    ~TravellingSalesmanProblem()= default;
    [[nodiscard]] const std::vector<std::vector<std::shared_ptr<double>>> &getCityDistances() const;
    [[nodiscard]] int getDim() const;
    [[nodiscard]] const std::vector<std::vector<std::shared_ptr<double>>> &getPheromones() const;
    std::vector<std::vector<std::shared_ptr<double>>>* getPheromonesPtr();
    std::vector<std::vector<std::shared_ptr<double>>>* getCityDistancesPtr();
    double getInitialPheromone() const;
};


#endif //ANTCOLONYOPTIMIZATION_TRAVELLINGSALESMANPROBLEM_H
