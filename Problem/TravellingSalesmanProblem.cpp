//
// Created by ivan on 31. 03. 2020..
//

#include "TravellingSalesmanProblem.h"
#include <cmath>

using namespace std;

TravellingSalesmanProblem::TravellingSalesmanProblem(string path) {
    this->reader = InstanceReader(path);
    vector<pair<double, double>> coordinates = reader.getCoordinates();
    this->dim = coordinates.size();
    loadData(coordinates);
}

void TravellingSalesmanProblem::loadData(vector<pair<double, double>> coordinates) {
    double max = 0;
    for (int i = 0; i < dim; i++){
        vector<shared_ptr<double>> v;
        cityDistances.push_back(v);
        for(int j = 0; j < dim; j++){
            double dist;
            if(i == j){
                dist = 0;
            } else {
                dist = distance(coordinates[i].first, coordinates[i].second, coordinates[j].first, coordinates[j].second);
            }
            shared_ptr<double> ptr(new double(dist));
            cityDistances[i].push_back(ptr);
            if(dist > max)
                max = dist;
        }
    }

    this->initialPheromone = 1 / max;

    for(int i = 0; i < dim; i++){
        vector<shared_ptr<double>> p;
        pheromones.push_back(p);
        for(int j = 0; j < dim; j++){
            shared_ptr<double> pheromone_ptr(new double(i != j ? initialPheromone : 0));
            pheromones[i].push_back(pheromone_ptr);
        }
    }
}

double TravellingSalesmanProblem::distance(double x1, double y1, double x2, double y2){
    double dx = x2 - x1;
    double dy = y2 - y1;
    return sqrt(dx * dx + dy * dy);
}

const vector<vector<shared_ptr<double>>> &TravellingSalesmanProblem::getCityDistances() const {
    return cityDistances;
}

int TravellingSalesmanProblem::getDim() const {
    return dim;
}

const vector<std::vector<std::shared_ptr<double>>> &TravellingSalesmanProblem::getPheromones() const {
    return pheromones;
}

std::vector<std::vector<std::shared_ptr<double>>> *TravellingSalesmanProblem::getPheromonesPtr() {
    return &pheromones;
}

std::vector<std::vector<std::shared_ptr<double>>> *TravellingSalesmanProblem::getCityDistancesPtr() {
    return &cityDistances;
}

double TravellingSalesmanProblem::getInitialPheromone() const {
    return initialPheromone;
}

