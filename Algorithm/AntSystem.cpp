//
// Created by ivan on 05. 04. 2020..
//

#include "AntSystem.h"

#include <memory>
#include <random>

using namespace std;

void AntSystem::run() {

    random_device rd;
    mt19937_64 engine(rd());
    uniform_int_distribution<int> dist(0, this->tsp.getDim() - 1);

    for (int i = 0; i < numberOfAnts; i++){
        this->ants.push_back(std::make_shared<CycleAnt>(i + 1, dist(engine), &m, &cvAnt, &cvMain, &cvWait, this->numberOfIterations,
                                                        numberOfAnts, this->tsp.getDim(), this->alpha, this->beta, tsp.getPheromones(), tsp.getCityDistances(), sync, syncWait));
    }

    auto start = std::chrono::high_resolution_clock::now();

    for(int iteration = 0; iteration < numberOfIterations; iteration++){

        {
            unique_lock<mutex> lock(m);
            cvMain.wait(lock, [this]{return *this->sync == this->numberOfAnts;});
            lock.unlock();      //???
        }
        {
            lock_guard <mutex> lg(m);
            cout << endl << "Updating pheromones" << endl << endl;
            reducePheromone();
            for(int i = 0; i < numberOfAnts; i++){
                vector<int> path = this->ants[i]->getPath();
                double length = this->ants[i]->getLength();
                updatePheromone(path, length);
                if(length < this->best || (iteration == 0 && i == 0)){
                    this->best = length;
                    this->bestPath = vector(path);
                    this->bestAnt = ants[i]->getIndex();
                }
            }
            printCurrentIteration();

            *sync = 0;
            *syncWait = 0;
            cvAnt.notify_all();
        }
    }

    auto stop = std::chrono::high_resolution_clock::now();

    this->duration = std::chrono::duration_cast<std::chrono::microseconds> (stop - start);

}


AntSystem::AntSystem(const TravellingSalesmanProblem& tsp) : AbstractAlgorithm(tsp) {
    this->tsp = tsp;
}

void AntSystem::updatePheromone(std::vector<int> path, double length) {
    vector<vector<shared_ptr<double>>> pheromones = tsp.getPheromones();
    int dim = (int) path.size() - 1;
    double additionalValue = 1 / length;
    for(int i = 0; i < dim; i++){
        int first = path[i];
        int second = path[i + 1];
        *pheromones[first][second] += additionalValue;
        *pheromones[second][first] += additionalValue;
    }
}
