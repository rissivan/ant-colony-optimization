//
// Created by ivan on 05. 04. 2020..
//

#ifndef ANTCOLONYOPTIMIZATION_ANTSYSTEM_H
#define ANTCOLONYOPTIMIZATION_ANTSYSTEM_H


#include "AbstractAlgorithm.h"

class AntSystem: public AbstractAlgorithm{
private:
    void updatePheromone(std::vector<int> path, double length);
public:
    AntSystem()= default;;
    explicit AntSystem(const TravellingSalesmanProblem& tsp);
    void run() override;
};


#endif //ANTCOLONYOPTIMIZATION_ANTSYSTEM_H
