//
// Created by ivan on 08. 05. 2020..
//

#include "MaxMinAntSystem.h"
#include "../Util/Constants.h"

#include <memory>
#include <random>

using namespace std;

void MaxMinAntSystem::run() {
    resetPheromones();

    random_device rd;
    mt19937_64 engine (rd());
    uniform_int_distribution<int> dist(0, this->tsp.getDim() - 1);


    for(int i = 0; i < numberOfAnts; i++){
        this->ants.push_back(make_shared<CycleAnt>(i + 1, dist(engine), &m, &cvAnt, &cvMain, &cvWait, this->numberOfIterations,
                numberOfAnts, this->tsp.getDim(), this->alpha, this->beta, tsp.getPheromones(), tsp.getCityDistances(), sync, syncWait));
    }


    auto start = chrono::high_resolution_clock::now();
    double lastBest = 0;
    int repeatCount = 0;

    for(int iteration = 0; iteration < numberOfIterations; iteration++){

        {
            unique_lock<mutex> lock(m);
            cvMain.wait(lock, [this]{return *this->sync == this->numberOfAnts;});
            lock.unlock();
        }
        {
            lock_guard<mutex> lg(m);
            cout << endl << "Updating pheromones" << endl << endl;
            reducePheromone();

            double iterationBest = 0;
            vector<int> iterationBestPath;
            int iterationBestAnt = 0;

            for(int i = 0; i < numberOfAnts; i++){
                vector<int> path = this->ants[i]->getPath();
                double length = this->ants[i]->getLength();

                if(length < this->best || (iteration == 0 && i == 0)){
                    this->best = length;
                    this->bestPath = vector(path);
                    this->bestAnt = ants[i]->getIndex();
                }

                if(length < iterationBest || (iteration == 0 && i == 0)){
                    iterationBest = length;
                    iterationBestPath = vector(path);
                    iterationBestAnt = ants[i]->getIndex();
                }
            }

            if(best < lastBest || lastBest == 0){
                lastBest = best;
                repeatCount = 0;
            } else {
                repeatCount++;
            }


            updatePheromone(bestPath, best);

            printCurrentIteration();

            if(repeatCount >= this->sameBestCount){
                lastBest = 0;
                repeatCount = 0;
                resetPheromones();
            }

            *sync = 0;
            *syncWait = 0;
            cvAnt.notify_all();
        }
    }

    auto stop = chrono::high_resolution_clock::now();

    this->duration = chrono::duration_cast<chrono::microseconds>(stop - start);

}

MaxMinAntSystem::MaxMinAntSystem(const TravellingSalesmanProblem &tsp) : AbstractAlgorithm(tsp) {
    this->tsp = tsp;
    this->minTau = MMAS_MIN;
    this->maxTau = MMAS_MAX;
    this->sameBestCount = this->numberOfIterations / 4;
}

void MaxMinAntSystem::setMinTau(double minTau) {
    MaxMinAntSystem::minTau = minTau;
}

void MaxMinAntSystem::setMaxTau(double maxTau) {
    MaxMinAntSystem::maxTau = maxTau;
}


void MaxMinAntSystem::updatePheromone(std::vector<int> path, double length) {
    vector<vector<shared_ptr<double>>> pheromones = tsp.getPheromones();
    int dim = (int) path.size() - 1;
    double additionalValue = (double) 1 / length;
    for(int i = 0; i < dim; i++){
        int first = path[i];
        int second = path[i + 1];
        *pheromones[first][second] += additionalValue;
        *pheromones[second][first] += additionalValue;
        if(*pheromones[first][second] > this->maxTau){
            *pheromones[first][second] = this->maxTau;
        }
    }
}

void MaxMinAntSystem::resetPheromones() {
    vector<vector<shared_ptr<double>>> pheromones = this->tsp.getPheromones();
    int dim = pheromones.size();
    for(int i = 0; i < dim; i++){
        for(int j = 0; j < dim; j++){
            *pheromones[i][j] = MMAS_MAX;
        }
    }
}

void MaxMinAntSystem::reducePheromone() {
    int dim = this->tsp.getDim();
    vector<vector<shared_ptr<double>>> pheromones = tsp.getPheromones();
    for(int i = 0; i < dim; i++){
        for(int j = i + 1; j < dim; j++){
            double newValue = *pheromones[i][j] * (1 - rho);
            if(newValue < this->minTau){
                newValue = this->minTau;
            }
            *pheromones[i][j] = newValue;
            *pheromones[j][i] = newValue;
        }
    }
}
