//
// Created by ivan on 08. 05. 2020..
//

#ifndef ANTCOLONYOPTIMIZATION_ANTCOLONYSYSTEM_H
#define ANTCOLONYOPTIMIZATION_ANTCOLONYSYSTEM_H


#include "AbstractAlgorithm.h"

class AntColonySystem : public AbstractAlgorithm{
private:
    double q0{};
    double phi{};
    void updatePheromone(std::vector<int> path, double length);
public:
    AntColonySystem()= default;;
    explicit AntColonySystem(const TravellingSalesmanProblem& tsp);
    void run() override;
    void setQ0(double q0);
    void setPhi(double phi);
};


#endif //ANTCOLONYOPTIMIZATION_ANTCOLONYSYSTEM_H
