//
// Created by ivan on 08. 05. 2020..
//

#ifndef ANTCOLONYOPTIMIZATION_MAXMINANTSYSTEM_H
#define ANTCOLONYOPTIMIZATION_MAXMINANTSYSTEM_H


#include "AbstractAlgorithm.h"

class MaxMinAntSystem: public AbstractAlgorithm {
private:
    double minTau{};
    double maxTau{};
    int sameBestCount;
    void updatePheromone(std::vector<int> path, double length);
    void resetPheromones();

protected:
    void reducePheromone() override;

public:
    MaxMinAntSystem()= default;;
    explicit MaxMinAntSystem(const TravellingSalesmanProblem& tsp);
    void setMinTau(double minTau);
    void setMaxTau(double maxTau);
    void run() override;
};


#endif //ANTCOLONYOPTIMIZATION_MAXMINANTSYSTEM_H
