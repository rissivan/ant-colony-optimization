//
// Created by ivan on 31. 03. 2020..
//

#ifndef ANTCOLONYOPTIMIZATION_ABSTRACTALGORITHM_H
#define ANTCOLONYOPTIMIZATION_ABSTRACTALGORITHM_H


#include <vector>
#include <mutex>
#include <condition_variable>
#include "../Ant/AbstractAnt.h"
#include "../Problem/TravellingSalesmanProblem.h"
#include "../Ant/CycleAnt.h"

class AbstractAlgorithm {
protected:
    int numberOfAnts;
    int numberOfIterations;
    double alpha;
    double beta;
    double rho;
    std::vector<std::shared_ptr<AbstractAnt>> ants;
    double best;
    std::vector<int> bestPath;
    int bestAnt;
    std::mutex m;
    std::condition_variable cvAnt, cvMain, cvWait;
    TravellingSalesmanProblem tsp;
    std::shared_ptr<int> sync, syncWait;
    std::chrono::microseconds duration;
    virtual void reducePheromone();
public:
    AbstractAlgorithm(){};
    explicit AbstractAlgorithm(TravellingSalesmanProblem tsp);
    virtual void run() = 0;
    void printStats();
    void setNumberOfAnts(int numberOfAnts);
    void setNumberOfIterations(int numberOfIterations);
    void setAlpha(double alpha);
    void setBeta(double beta);
    void setRho(double rho);
    void printCurrentIteration();

};


#endif //ANTCOLONYOPTIMIZATION_ABSTRACTALGORITHM_H
