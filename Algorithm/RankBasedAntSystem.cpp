//
// Created by ivan on 07. 05. 2020..
//

#include "RankBasedAntSystem.h"
#include "../Util/Constants.h"

#include <memory>
#include <random>

using namespace std;

struct RankedPath{
    vector<int> path;
    double length{};
    int antIndex{};

    RankedPath()= default;;
    RankedPath(const vector<int> &path, double length, int antIndex) : path(path), length(length), antIndex(antIndex) {}

    bool operator<(const RankedPath &rhs) const {
        return length < rhs.length;
    }
};

void RankBasedAntSystem::run() {

    random_device rd;
    mt19937_64 engine (rd());
    uniform_int_distribution<int> dist(0, this->tsp.getDim() - 1);

    for(int i = 0; i < numberOfAnts; i++){
        this->ants.push_back(make_shared<CycleAnt>(i + 1, dist(engine), &m, &cvAnt, &cvMain, &cvWait, this->numberOfIterations,
                numberOfAnts, this->tsp.getDim(), this->alpha, this->beta, tsp.getPheromones(), tsp.getCityDistances(), sync, syncWait));
    }

    auto start = chrono::high_resolution_clock::now();

    for(int iteration = 0; iteration < numberOfIterations; iteration++){

        {
            unique_lock<mutex> lock(m);
            cvMain.wait(lock, [this]{return *this->sync == this->numberOfAnts;});
            lock.unlock();
        }
        {
            lock_guard<mutex> lg(m);
            cout << endl << "Updating pheromones" << endl << endl;
            reducePheromone();
            for(int i = 0; i < numberOfAnts; i++){
                vector<int> path = this->ants[i]->getPath();
                double length = this->ants[i]->getLength();
                if(length < this->best || (iteration == 0 && i == 0)){
                    this->best = length;
                    this->bestPath = vector(path);
                    this->bestAnt = ants[i]->getIndex();
                }
            }

            set<RankedPath> rankedPaths;
            for(int i = 0; i < numberOfAnts; i++){
                vector<int> path = this->ants[i]->getPath();
                double length = this->ants[i]->getLength();
                if(ants[i]->getIndex() != bestAnt)
                    rankedPaths.insert(RankedPath(path, length, ants[i]->getIndex()));
            }

            updatePheromone(ants[bestAnt - 1]->getPath(), ants[bestAnt - 1]->getLength(), weight);
            for(int i = 0; i < this->weight - 1; i++){
                updatePheromone(ants[i]->getPath(), ants[i]->getLength(), weight - 1 - i);
            }

            printCurrentIteration();

            *sync = 0;
            *syncWait = 0;
            cvAnt.notify_all();
        }
    }

    auto stop = chrono::high_resolution_clock::now();

    this->duration = chrono::duration_cast<chrono::microseconds>(stop - start);

}

RankBasedAntSystem::RankBasedAntSystem(const TravellingSalesmanProblem& tsp) : AbstractAlgorithm(tsp) {
    this->tsp = tsp;
    this->weight = AS_RANK_WEIGHT_DEFAULT;
}

void RankBasedAntSystem::setWeight(int w) {
    RankBasedAntSystem::weight = w;
}

void RankBasedAntSystem::updatePheromone(std::vector<int> path, double length, int w) {
    vector<vector<shared_ptr<double>>> pheromones = this->tsp.getPheromones();
    int dim = (int) path.size() - 1;
    double additionalValue = (double) w / length;
    for(int i = 0; i < dim; i++){
        int first = path[i];
        int second = path[i + 1];
        *pheromones[first][second] += additionalValue;
        *pheromones[second][first] += additionalValue;
    }

}
