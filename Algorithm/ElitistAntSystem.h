//
// Created by ivan on 07. 05. 2020..
//

#ifndef ANTCOLONYOPTIMIZATION_ELITISTANTSYSTEM_H
#define ANTCOLONYOPTIMIZATION_ELITISTANTSYSTEM_H


#include "AbstractAlgorithm.h"

class ElitistAntSystem: public AbstractAlgorithm {
private:
    void updatePheromone(std::vector<int> path, double length);
public:
    ElitistAntSystem()= default;;
    ElitistAntSystem(TravellingSalesmanProblem tsp);
    void run() override;
};


#endif //ANTCOLONYOPTIMIZATION_ELITISTANTSYSTEM_H
