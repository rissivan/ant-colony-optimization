//
// Created by ivan on 31. 03. 2020..
//

#include "AbstractAlgorithm.h"
#include "../Util/Constants.h"

#include <memory>

using namespace std;

AbstractAlgorithm::AbstractAlgorithm(TravellingSalesmanProblem tsp) {
    this->sync = std::make_shared<int>();
    *this->sync = 0;
    this->syncWait = std::make_shared<int>();
    *syncWait = 0;
    this->numberOfAnts = NO_ANTS_DEFAULT;
    this->numberOfIterations = NO_ITERATIONS_DEFAULT;
    this->alpha = ALPHA_DEFAULT;
    this->beta = BETA_DEFAULT;
    this->rho = RHO_DEFAULT;
    this->best = 0;
}

void AbstractAlgorithm::setNumberOfAnts(int noAnts) {
    AbstractAlgorithm::numberOfAnts = noAnts;
}

void AbstractAlgorithm::setNumberOfIterations(int noIterations) {
    AbstractAlgorithm::numberOfIterations = noIterations;
}

void AbstractAlgorithm::setAlpha(double a) {
    AbstractAlgorithm::alpha = a;
}

void AbstractAlgorithm::setBeta(double b) {
    AbstractAlgorithm::beta = b;
}

void AbstractAlgorithm::setRho(double r) {
    AbstractAlgorithm::rho = r;
}

void AbstractAlgorithm::printCurrentIteration() {
    cout << "----------------------------------------------" << endl;
    cout << "Best path of length " << best << " found by ant " << bestAnt << ":" << endl << "[";
    int dim = tsp.getDim();
    for(int i = 0; i < dim; i++){
        cout << bestPath[i] << ((i < dim - 1) ? " " : "]");
    }
    cout << endl;
    cout << "----------------------------------------------" << endl << endl;
}

void AbstractAlgorithm::printStats() {
    int dim = this->tsp.getDim();
    cout << "Results:" << endl;
    cout << "=========================================================================" << endl;
    cout << "Number of cities: " << dim << endl;
    cout << "Best value: " << this->best << endl;
    cout << "Best path: " << "[";
    for(int i = 0; i < dim; i++){
        cout << this->bestPath[i] << ((i < dim - 1) ? " " : "]");
    }
    cout << endl;
    cout << "Execution time: " << (double) duration.count() / 1000000 << " seconds." << endl;
    cout << "=========================================================================" << endl;
}

void AbstractAlgorithm::reducePheromone() {
    int dim = this->tsp.getDim();
    vector<vector<shared_ptr<double>>> pheromones = tsp.getPheromones();
    for(int i = 0; i < dim; i++){
        for(int j = i + 1; j < dim; j++){
            double newValue = *pheromones[i][j] * (1 - rho);
            *pheromones[i][j] = newValue;
            *pheromones[j][i] = newValue;
        }
    }
}
