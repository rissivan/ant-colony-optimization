//
// Created by ivan on 07. 05. 2020..
//

#ifndef ANTCOLONYOPTIMIZATION_RANKBASEDANTSYSTEM_H
#define ANTCOLONYOPTIMIZATION_RANKBASEDANTSYSTEM_H


#include "AbstractAlgorithm.h"

class RankBasedAntSystem: public AbstractAlgorithm {
private:
    int weight{};
    void updatePheromone(std::vector<int> path, double length, int weight);
public:
    RankBasedAntSystem()= default;;
    explicit RankBasedAntSystem(const TravellingSalesmanProblem& tsp);
    void setWeight(int weight);
    void run() override;
};


#endif //ANTCOLONYOPTIMIZATION_RANKBASEDANTSYSTEM_H
